# Cutlist Calculator

Hi there! You probably know me as [whistlewright](https://liberdon.com/web/accounts/27233).
If you are in the Fediverse somewhere you can follow me there.

I am trying to make a parts list generator for my cabinet making work 
using JavaScript. Truth is, that this project is more about learning 
JavaScript than it is about making a pretty project.

## Roadmap
* Need to add logic for face frame and bodies buttons (I have functionality
on this now. It will make a list but I have to do all the ciphering. I am
sure that it'll take a bajillion lines of repetitive code to make the computer 
figure this stuff out. Going to try it anyway.)
* figure out how to convert decimals to fractions (another large project???)
* use JS to add the svg files to the doc. 
* ~~add note so I can remember the copy command (not sure what this is. It
should be a simple matter of ctrl-p -> save as pdf but maybe I wanted to 
save the generated html???)~~ I need to be able to save the JS created HTML
in caser it needs editing later due to changes and/or mistakes. This is 
simple. In the browser hit CTRL-s and save as 'webpage complete.' There 
might be a way to do this with JS and a button but I am not sure yet. 
* ~~need the sidebar to disappear after the cutlist is generated~~ WHO'S YER DADDY?!
* ~~do the running count thing on the door and drawer lists~~ I AM YER DADDY!
* Forgot about the Job Info sheet. I need to add this one in too.
* When I get this all ciphered out I'll redo it for node. It might be 
better as a nodejs project.


